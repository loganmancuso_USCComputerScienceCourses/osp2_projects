# *************************************************************
# 'compile.sh'
# this program will compile the program and output errors to
# a text file called xout.txt
#
# Author/CopyRight: Mancuso, Logan
# Last Edit Date: 10-10-2017--17:02:42
# *************************************************************

#!/bin/bash

# compile program
# ./zaRefreshDate.sh
 javac -g -classpath .:OSP.jar: -d . *.java |& tee xout.txt
# *************************************************************
# End 'compile.sh'
# *************************************************************
