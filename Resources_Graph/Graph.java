/****************************************************************
 * 'Graph.java'
 *
 * Author/CopyRight: Mancuso, Logan
 * Last Edit Date: 11-27-2017--18:36:48
 *
**/

/****************************************************************
 * Imports and Packages
**/
import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

/****************************************************************
 * Interfaces
 * 'Visitor'
 * 'VisitorEX'
 * 'DFSVisitor'
**/

interface Visitor<Type> {
  public void visit(Graph<Type> graph, Vertex<Type> vertex);
}//end Interface Visitor

interface VisitorEX<Type, E extends Exception> {
  public void visit(Graph<Type> graph, Vertex<Type> vertex) throws Exception;
}//end Interface VisitorEX

interface DFSVisitor<Type> {
  public void visit(Graph<Type> graph, Vertex<Type> vertex);
  public void visit(Graph<Type> graph, Vertex<Type> vertex, Edge<Type> edge);
}

/****************************************************************
 * Class 'Graph'
 *
**/
public class Graph<Type> {
  /****************************************************************
   * Internal Variables
   * 
   * Final Int
   * VISIT_COLOR_WHITE
   * VISIT_COLOR_GREY 
   * VISIT_COLOR_BLACK
   * 
   * List
   * vertices_
   * edges_
   * 
   * Vertex:
   * root_vertex_
  **/
  public static final int VISIT_COLOR_WHITE = 1;
  public static final int VISIT_COLOR_GREY = 2;
  public static final int VISIT_COLOR_BLACK = 3;

  private List<Vertex<Type>> vertices_;
  private List<Edge<Type>> edges_;
  private Vertex<Type> root_vertex_;

  /****************************************************************
   * Constructor
  **/
  public Graph() {
    vertices_ = new ArrayList<Vertex<Type>>();
    edges_ = new ArrayList<Edge<Type>>();
  }

  /****************************************************************
   * Param Constructor
  **/
  public Graph( Vertex<Type> root ) {
    vertices_ = new ArrayList<Vertex<Type>>();
    edges_ = new ArrayList<Edge<Type>>();
    set_root_vertex( root );
  }

  /****************************************************************
   * Accessors
   * 'get_root_vertex'
   * 'get_vertex'
   * 'get_vertex' (name)
   * 'get_vertex' (data)
   * 'get_vertices'
   * 'get_edge'
   * 'get_edges'
   * 
  **/

  public Vertex<Type> get_root_vertex() {
    return this.root_vertex_;
  }//end get+root_vertex

  public Vertex<Type> get_vertex( int here ) {
    return this.vertices_.get( here );
  }//end get_vertices
  
  public Vertex<Type> get_vertex( String name ) {
    Vertex<Type> match = null;
    for( Vertex<Type> this_vertex : this.vertices_ ) {
      if( name.equals( this_vertex.get_name() ) ) {
        match = this_vertex;
        break;
      }//end if
    }//end for
    return match;
  }//end get_vertex

  public Vertex<Type> get_vertex( Type data, Comparator<Type> compare ) {
    Vertex<Type> match = null;
    for( Vertex<Type> this_vertex : this.vertices_ ) {
      if( compare.compare( data, this_vertex.get_data() ) == 0 ) {
        match = this_vertex;
        break;
      }//end if
    }//end for
    return match;
  }//end get_vertex

  public List<Vertex<Type>> get_vertices() {
    return this.vertices_;
  }//end get_vertices

  public Edge<Type> get_edge( Vertex<Type> from, Vertex<Type> to ) {
    for( Edge<Type> an_edge : this.edges_ ) {
      if( ( an_edge.get_previous().equals( from ) ) 
        &&( an_edge.get_next().equals( to ) ) ) 
      {
        return an_edge;
      }//end if
    }//end for
    return null;
  }

  public List<Edge<Type>> get_edges() {
    return this.edges_;
  }//end get_edges

  /****************************************************************
   * Mutators
   * 'set_root_vertex'
  **/
  public void set_root_vertex( Vertex<Type> root ) {
    this.root_vertex_ = root;
    if( !this.vertices_.contains( root ) ) {
      this.vertices_.add( root );
    }//end if
  }//end set_root_vertex

  /****************************************************************
   * Functions
   * 'is_empty'
   * 'add_vertex'
   * 'size'
   * 'add_edge'
   * 'insert_bi_edge'
   * 'remove_vertex'
   * 'remove_edge'
   * 'clear_mark'
   * 'clear_edges'
   * 'dfs'
   * 'bfs'
   * 'dfs_tree'
   * 'find_cycle'
   * 'visit'
   * 'to_string'
  **/

  public boolean is_empty() {
    return this.vertices_.size() == 0;
  }//end is_empty

  public boolean add_vertex( Vertex<Type> vertex ) {
    boolean added = false;
    if( !this.vertices_.contains( vertex ) ) {
      added = this.vertices_.add( vertex );
    }//end if
    return added;
  }//end for

  public int size() {
    return this.vertices_.size();
  }//end size

  public boolean add_edge( Vertex<Type> from, Vertex<Type> to, int cost ) throws IllegalArgumentException {
    if( !this.vertices_.contains( from )
      || !this.vertices_.contains( to ) )
    {
      throw new IllegalArgumentException( "Vertex is not in the graph" );
    }//end if
    Edge<Type> edge = new Edge<Type>( from, to, cost );
    if( from.find_edge( to ) != null ) {
      return false;
    }//end if
    else {
      from.add_edge( edge );
      to.add_edge( edge );
      edges_.add( edge );
      return true;
    }//end else
  }//end add_edge

  public boolean insert_bi_edge( Vertex<Type> from, Vertex<Type> to, int cost ) throws IllegalArgumentException {
    return add_edge( from, to, cost ) && add_edge( to, from , cost );
  }//end insert_bi_edge

  public boolean remove_vertex( Vertex<Type> vertex ) {
    if( !this.vertices_.contains( vertex ) ) {
      return false;
    }//end if
    this.vertices_.remove( vertex );
    if( vertex == this.root_vertex_ ) {
      this.root_vertex_ = null;
    }//end if
    for( int i = 0; i < vertex.get_outgoing_edge_count(); i++ ) {
      Edge<Type> edge = vertex.get_outgoing_edge( i );
      vertex.remove( edge );
      Vertex<Type> to = edge.get_next();
      to.remove( edge );
      this.edges_.remove( edge );
    }//end for
    for( int i = 0; i < vertex.get_incoming_edge_count(); i++ ) {
      Edge<Type> edge = vertex.get_incoming_edge( i );
      vertex.remove( edge );
      Vertex<Type> predecessor = edge.get_previous();
      predecessor.remove( edge );
    }//end for
    return true;
  }//end remove_vertex

  public boolean remove_edge( Vertex<Type> from, Vertex<Type> to ) {
    Edge<Type> edge = from.find_edge( to );
    if( edge == null ) {
      return false;
    }//end if
    else {
      from.remove( edge );
      to.remove( edge );
      edges_.remove( edge );
      return true;
    }//else
  }//end remove_edge

  public void clear_mark() {
    for( Vertex<Type> w : this.vertices_ ) {
      w.clear_mark();
    }//end for
  }//end clear_mark

  public void clear_edges() {
    for( Edge<Type> edge : this.edges_ ) {
      edge.clear_mark();
    }//end for
 }//end clear_edges

  public void dfs( Vertex<Type> vertex, final Visitor<Type> visitor ) {
    VisitorEX<Type, RuntimeException> wrapper = new VisitorEX<Type, RuntimeException>() {
      public void visit( Graph<Type> graph, Vertex<Type> vertex ) throws RuntimeException {
        if( visitor != null ) {
          visitor.visit( graph, vertex );
        }//end if
      }//end visit
    };//end wrapper
    this.dfs( vertex, wrapper );
  }//end dfs

  public <E extends Exception> void dfs( Vertex<Type> vertex, VisitorEX<Type, E> visitor ) throws E {
    try {
      if( visitor != null ) { 
        visitor.visit( this, vertex );
      }//end if
    }//end try
    catch (Exception exception ) {
      System.out.println( exception.getMessage() );
    }//end catch
    vertex.visit();
    for( int i = 0; i < vertex.get_outgoing_edge_count(); i ++ ) {
      Edge<Type> edge = vertex.get_outgoing_edge( i );
      if( !edge.get_next().visited() ) {
        dfs( edge.get_next(), visitor );
      }//end if
    }//end for
  }//end dfs

  public void bfs( Vertex<Type> vertex, final Visitor<Type> visitor ) {
    VisitorEX<Type, RuntimeException> wrapper = new VisitorEX<Type, RuntimeException>() {
      public void visit( Graph<Type> graph, Vertex<Type> vertex ) throws RuntimeException {
        if( visitor != null ) {
          visitor.visit( graph, vertex );
        }//end if
       }//end visit
    };//end wrapper
    this.bfs( vertex, wrapper );
  }//end bfs

  public <E extends Exception> void bfs( Vertex<Type> vertex, VisitorEX<Type, E> visitor ) throws E {
    LinkedList<Vertex<Type>> queue = new LinkedList<Vertex<Type>>();
    queue.add( vertex );
    try {
      if (visitor != null) {
        visitor.visit( this, vertex );
      }//end if
    }//end try
    catch (Exception exception) {
      System.out.println(exception.getMessage());
    } //end catch
    vertex.visit();
    while( queue.isEmpty() == false ) {
      vertex = queue.removeFirst();
      for( int i  = 0; i < vertex.get_outgoing_edge_count(); i++ ) {
        Edge<Type> an_edge = vertex.get_outgoing_edge( i );
        Vertex<Type> to = an_edge.get_next();
        if( !to.visited() ) {
          queue.add( to );
          try {
            if (visitor != null) {
              visitor.visit(this, vertex);
            } //end if
          } //end try
          catch (Exception exception) {
            System.out.println(exception.getMessage());
          } //end catch
          to.visit();
        }//end if
      }//end for i
    }//end while
  }//end bfs

  public void dfs_tree( Vertex<Type> vertex, DFSVisitor<Type> visitor ) {
    vertex.visit();
    try {
      if (visitor != null) {
        visitor.visit(this, vertex);
      } //end if
    } //end try
    catch (Exception exception) {
      System.out.println(exception.getMessage());
    } //end catch
    for( int i = 0; i < vertex.get_outgoing_edge_count(); i++ ) {
      Edge<Type> an_edge = vertex.get_outgoing_edge( i );
      if( !an_edge.get_next().visited() ) {
        try {
          if (visitor != null) {
            visitor.visit(this, vertex, an_edge );
          } //end if
          an_edge.mark();
          dfs_tree( an_edge.get_next(), visitor );
        } //end try
        catch (Exception exception) {
          System.out.println(exception.getMessage());
        } //end catch
      }//end if
    }//end for i
  }//end dfs_tree

  public List<Edge<Type>> find_cycle() {
    ArrayList<Edge<Type>> cycle_edges = new ArrayList<Edge<Type>>();
    for( int i = 0; i < this.vertices_.size(); i++ ) {
      Vertex<Type> vertex = this.get_vertex( i );
      vertex.set_marked_state( VISIT_COLOR_WHITE );
    }//end for i
    for( int i = 0; i < this.vertices_.size(); i++ ) {
      Vertex<Type> vertex = this.get_vertex( i );
      visit( vertex, cycle_edges );
    }//end for
    return cycle_edges;
  }//end find_cycle
  
  // public Set<Vertex<Type>> vertices_in_cycle() {
  //   for( Vertex<Type> vertex : this.vertices_ ) {
  //     find_all_vertices( vertex );
  //   }//end for
  // }//end vertices_in_cycle
  // private Vertex<Type> find_all_vertices( Vertex<Type> vertex ) {
  //   for( Vertex<Type> this_vertex : )
  // }

  private void visit( Vertex<Type> vertex, ArrayList<Edge<Type>> cycle_edges ) {
    vertex.set_marked_state( VISIT_COLOR_GREY );
    for( int i = 0; i < vertex.get_outgoing_edge_count(); i++ ) {
      Edge<Type> an_edge = vertex.get_outgoing_edge( i );
      Vertex<Type> a_vertex = an_edge.get_next();
      if( a_vertex.get_marked_state() == VISIT_COLOR_GREY ) {
        cycle_edges.add( an_edge );
      }//end if
      else if ( a_vertex.get_marked_state() ==VISIT_COLOR_WHITE ) {
        visit( a_vertex, cycle_edges );
      }//end else if
    }//end for
    vertex.set_marked_state( VISIT_COLOR_BLACK );
  }//end visit




  public String to_string() {
    if( this.vertices_.isEmpty() ) {
      return "Graph is Empty";
    }//end if
    else {
      StringBuffer out_string = new StringBuffer();
      out_string.append("Vertices:\n");
      for( Vertex<Type> a_vertex : this.vertices_ ) {
        out_string.append( a_vertex.to_string() + "\n" );
      }//end for
      return out_string.toString();
    }//end else
  }//end to_string

}//end Graph

/****************************************************************
 * End 'Graph.java'
**/
