/****************************************************************
 * 'ThreadCB.java'
 *
 * Author/CopyRight: Mancuso, Logan
 * Last Edit Date: 10-10-2017--17:02:42
 * Room: 3A11
 * Sources: gitHub and text book
 * including source code and documentation provided by
 * the text book for understanding and implementing methods
 * and for creating and setting functions. web resources used
 * were for reference to uses pertaining to the Generic List and
 * the functions associated with it and the creation of threads
 * in a queued system.
 *
**/

/****************************************************************
 * Imports and Packages
**/
package osp.Threads;
import java.util.Vector;
import java.util.Enumeration;
import org.omg.CORBA.COMM_FAILURE;
import osp.Utilities.*;
import osp.IFLModules.*;
import osp.Tasks.*;
import osp.EventEngine.*;
import osp.Hardware.*;
import osp.Devices.*;
import osp.Memory.*;
import osp.Resources.*;

/****************************************************************
 * This class is responsible for actions related to a thread,
 * including: creating, killing, dispatching, resuming, and
 * suspending threads.
**/

public class ThreadCB extends IflThreadCB {

  /****************************************************************
   * Variables
  **/
  private static GenericList hp_queue_; //high priority queue for threads
  private static GenericList lp_queue_; //low priority queue
  private static int prev_CPU_burst; //the previous cpu burst
  private static int calc_burst_; //the calculated burst
  private static long prev_dispatch; //the previous dispatch time (long)
  static int tau_n = 5; //tau for clock adjustment
  static double alpha_ = 0.75; //given alpha value

  /****************************************************************
   * Function 'ThreadCB'
   * function calls parent class to implement functions
  **/
  public ThreadCB() {
    super(); //calls parent class
    //10 flag for new threads
    this.prev_dispatch = 10;
    this.calc_burst_ = 10;
    this.prev_CPU_burst = 10;
  }//end ThreadCB

  /****************************************************************
   * Function 'init'
   * initialize the two queues for the threads using a built-in
   * generic queue. The high queue will manage the priority
   * queue of threads and the low priorty one will manage ones
   * that have ran before
  **/
  public static void init() {
    hp_queue_ = new GenericList(); //initialize
    lp_queue_ = new GenericList(); //intitialize
  }//end init

  /****************************************************************
   * Function 'do_create'
   * this function will create a thread given some task, from this
   * it will check the thread count and determine if there needs
   * to be a new thread dispatched. If it cannot add it will
   * dispatch a new thread and run the task
  **/
  static public ThreadCB do_create(TaskCB task) {
    if( task == null ) {
    //  System.out.println("task null");
      dispatch();
      return null;
    }//end if
    if( task.getThreadCount() > MaxThreadsPerTask ) {
    //  System.out.println("task full");
      dispatch();
      return null;
    }//end if
    ThreadCB thread_ = new ThreadCB(); //call new thread
    thread_.setPriority(task.getPriority());
    thread_.setStatus(ThreadReady);
    thread_.setTask(task);
    if( task.addThread(thread_) != SUCCESS ) { //if cannot place
      // System.out.println("cannot add");
      dispatch();
      return null;
    }//end if
    hp_queue_.append(thread_); //add to end of queue
    dispatch();
    return thread_;
  }//end do_create

  /****************************************************************
   * Function 'do_kill'
   * This function is responsible for removing a thread from the
   * queue and from killing the thread from running once complete.
   * It will determine the running state from the status method in
   * the thread class.
  **/
  public void do_kill() {
   // ThreadCB thread_ = null;
    if( this.getStatus() == ThreadRunning ) {
      try {
      ThreadCB thread_ = MMU.getPTBR().getTask().getCurrentThread();
        if( thread_ == this ) {
          MMU.setPTBR(null);
          getTask().setCurrentThread(null);
        }//end if
      }//end try
      catch( NullPointerException exception ) {
      //  System.out.println("Null Pointer Expection in function \'do_kill\'");
      }//end catch
    }//end if
    if( this.getStatus() == ThreadReady ) {
      // System.out.println("thread ready status");
      if( hp_queue_.contains(this) ) {
        hp_queue_.remove(this);
      }//end if
      else if( lp_queue_.contains(this) ) {
        lp_queue_.remove(this);
      }//end else if
    }//end else if
    if (this.getTask().removeThread(this) != SUCCESS ) {
      return;
    }//end if
    else {
      this.getTask().removeThread(this); //remove thread from task list
    }//end else
    this.setStatus(ThreadKill); //kill thread
    for( int i = 0; i < Device.getTableSize(); i++ ) {
      Device.get(i).cancelPendingIO(this);
    }//end for
    ResourceCB.giveupResources(this); //remove unused resources
    dispatch();
    if( this.getTask().getThreadCount() == 0 ) {
      this.getTask().kill();
      // System.out.println("threads complete killing task");
    }//end if
  }//end do_kill

  /****************************************************************
   * Function 'do_suspends'
   * this function will stop the thread if it is running. I will
   * evaluate the waiting status and if it is in the queue.
   * High or Low queue
  **/
  public void do_suspend(Event event) {
    if( this.getStatus() == ThreadRunning ) {
      try {
        ThreadCB thread_ = MMU.getPTBR().getTask().getCurrentThread();
        if( this == thread_ ) {
          MMU.setPTBR(null);
          this.getTask().setCurrentThread(null);
          setStatus(ThreadWaiting);
          //recalculate since change in status from running
          this.calc_burst_ = calcTau(this);
          this.prev_CPU_burst = (int) HClock.get() - (int) thread_.prev_dispatch;
          // System.out.println("thread set to waiting status");
        }//end if
      }//end try
      catch( NullPointerException exception ) {
      //  System.out.println("Null Pointer Expection in function \'do_suspends\'");
      }//end catch
    }//end if
    //is a waiting thread
    else if( this.getStatus() >= ThreadWaiting ) {
      this.setStatus(this.getStatus() + 1);
      // System.out.println("set status + 1 " + getStatus());
    }//end if
    if( hp_queue_.contains(this) ) {
      hp_queue_.remove(this);
      // System.out.println("removed from hp queue");
    }//end if
    else if( lp_queue_.contains(this) ) {
      lp_queue_.remove(this);
      // System.out.println("removed from lp queue");
    }//end else if
    event.addThread(this);
    dispatch();
  }//end do_suspends

  /****************************************************************
   * Function 'do_resume'
   * this function will resume the thread after comparing the
   * thread waiting is called. It pulls the value of the thread
   * and will change the status if it had run
  **/
  public void do_resume() {
    if( this.getStatus() < ThreadWaiting ) {
      return; //thread is not waiting
    }//end if
    else if( this.getStatus() == ThreadWaiting ) {
      this.setStatus(ThreadReady);
      // System.out.println("added thread back to queue");
    }//end else if
    else if( this.getStatus() > ThreadWaiting) {
      this.setStatus(this.getStatus() - 1);
      // System.out.println("set status - 1 " + getStatus());
    }//end else if
    if( this.getStatus() == ThreadReady ) {
      if( (this.prev_CPU_burst <= tau_n)
       || (this.prev_CPU_burst == 10) ) {
        hp_queue_.append(this);
      }//end if
      else {
        lp_queue_.append(this);
      }//end else
      this.prev_CPU_burst = (int) HClock.get() - (int) this.prev_dispatch;
    }//end if
    dispatch();
  }//end do_resume

  /****************************************************************
   * Function 'do_dispatch'
   * this function will dispatch a thread and let it run passing
   * the values of the task to the thread and running. It will
   * stop the CPU to check the values and add to the queue
  **/
  public static int do_dispatch() {
    ThreadCB thread_ = null;
    try {
      thread_ = MMU.getPTBR().getTask().getCurrentThread();
    }//end try
    catch( NullPointerException exception ) {
    //  System.out.println("Null Pointer Expection in function \'do_dispatch\'");
    }//end catch
    if( thread_ != null ) {
      thread_.getTask().setCurrentThread(null);
      MMU.setPTBR(null);
      thread_.setStatus(ThreadReady);
      if( (thread_.prev_CPU_burst <= tau_n)
       || (thread_.prev_CPU_burst == 10) ) {
          hp_queue_.append(thread_);
       }//end if
       else {
        lp_queue_.append(thread_);
       }//end else
       thread_.prev_CPU_burst = (int) HClock.get() - (int) thread_.prev_dispatch;
    }//end if
    ThreadCB new_thread_ = (ThreadCB) hp_queue_.removeHead();
    if( new_thread_ != null ) {
      MMU.setPTBR (new_thread_.getTask().getPageTable());
      new_thread_.getTask().setCurrentThread(new_thread_);
      new_thread_.setStatus(ThreadRunning);
    //  HTimer.set(calcTau(new_thread_));
     // new_thread_.calc_burst_ = calcTau(new_thread_);
    //  new_thread_.prev_CPU_burst = calcTau(new_thread_);
      new_thread_.prev_dispatch = HClock.get();
      return SUCCESS;
    }//end if
    else {
      new_thread_ = (ThreadCB) lp_queue_.removeHead();
      if( new_thread_ != null ) {
        MMU.setPTBR(new_thread_.getTask().getPageTable());
        new_thread_.getTask().setCurrentThread(new_thread_);
        new_thread_.setStatus(ThreadRunning);
      //  HTimer.set(calcTau(new_thread_));
      //  new_thread_.calc_burst_ = calcTau(new_thread_);
        new_thread_.prev_dispatch = HClock.get();
        return SUCCESS;
      }//end if
      else {
        MMU.setPTBR(null);
        return FAILURE;
      }//end else
    }//end else
  }//end do_dispatch

  /****************************************************************
   * Function 'atError'
   * prints error message when the program has encountered an
   * error
  **/
  public static void atError() {
    System.out.println("An Error Has Occurred");
  }//end atError

  /****************************************************************
   * Function 'atWarning'
   * prints a warning when the program has encountered a
   * warning
  **/
  public static void atWarning() {
    System.out.println("A Warning For The Program");
  }//end atWarning

  /****************************************************************
   * Function 'calcTau'
  **/
  public static int calcTau(ThreadCB thread_) {
    //if the prev burst (tau_n) is less than defined then return
    //tau_n else take the given value
    int prev_CPU = thread_.prev_CPU_burst < tau_n ?
                    tau_n : thread_.prev_CPU_burst;
    return ( (int) (alpha_ * prev_CPU) + (int) ( (1-alpha_) * prev_CPU) );
  }//end calcTau

}//end class ThreadCB

/****************************************************************
 * End 'ThreadCB.java'
**/