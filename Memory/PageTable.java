/****************************************************************
 * 'PageTable.java'
 *
 * Author/CopyRight: Mancuso, Logan
 * Last Edit Date: 11-29-2017--08:57:12
 *
**/

/****************************************************************
 * Imports and Packages
**/
package osp.Memory;
import java.lang.Math;
import osp.Tasks.*;
import osp.Utilities.*;
import osp.IFLModules.*;
import osp.Hardware.*;

/****************************************************************
 * Class
**/
public class PageTable extends IflPageTable {

  /****************************************************************
   * Param Constructor
  **/
  public PageTable(  TaskCB a_task  ) {
    super(  a_task  );
    int number_of_pages = ( int )Math.pow( 2, MMU.getPageAddressBits() );
    pages = new PageTableEntry[number_of_pages];
    for( int i = 0; i < number_of_pages; i++ ) {
      pages[i] = new PageTableEntry( this, i );
    }//end for i
  }//end PageTable

  /****************************************************************
   * Functions
   * 'do_deallocateMemory'
  **/
  
  public void do_deallocateMemory() {
    TaskCB task = getTask();
    for( int i = 0; i < MMU.getFrameTableSize(); i++ ) {
      FrameTableEntry tmp_frame_table_entry = MMU.getFrame( i );
      PageTableEntry tmp_page_table_entry = tmp_frame_table_entry.getPage();
      if( tmp_page_table_entry != null && tmp_page_table_entry.getTask() == task ) {
        tmp_frame_table_entry.setPage( null );
        tmp_frame_table_entry.setDirty( false );
        tmp_frame_table_entry.setReferenced( false );
        if( tmp_frame_table_entry.getReserved() == task ) {
          tmp_frame_table_entry.setUnreserved( task );
        }//end if
      }//end if
    }//end for
  }//end do_deallocateMemory

}//end class PageTable

/****************************************************************
 * End 'PageTable.java'
**/
